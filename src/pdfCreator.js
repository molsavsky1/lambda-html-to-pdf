'use strict';

const wkhtmltopdf = require('wkhtmltopdf');
const fs = require('fs');
const uuid = require('uuid');
const AWS = require('aws-sdk');
const moment = require('moment');

const utils = require('./utils');

const S3 = new AWS.S3();

const OUTPUT_TMP_FOLDER = '/tmp';

const FILENAME_DATE_FORMAT = 'YYYY_MM_DD_HH_mm_ss';

/**
 * Class handling pdf rendering using whtmltopdf binary
 */
class PDFCreator {
    /**
     * @param {String} bucket S3 bucket name for storing rendered pdfs
     * (Lambda has limit to 6MB per response)
     * @param {Number} expiryTime Number of seconds for S3 signed link
     * that is returned as a response
     * @param {Object} defaultRendererOptions WKHtmlToPdf rendering options
     */
    constructor(bucket, expiryTime, defaultRendererOptions) {
        this.s3_bucket = bucket;
        this.expiryTime = expiryTime;
        this.defaultRendererOptions = defaultRendererOptions;
    }

    /**
     * @param {String} content URL or HTML string to render
     * @param {String?} filename Optional filename
     * @param {String?} dateUpdated Optional date of last update
     * @param {Boolean} disableCache Disables caching
     * @param {Object?} options WKHtmlToPdf plugin options
     * @return {Promise<String>}
     */
    async render(content, filename = null, dateUpdated = null, disableCache = false, options = {}) {
        let momentDate = moment();

        if (dateUpdated != null) {
            momentDate = moment(dateUpdated, 'YYYY-MM-DD HH:mm:ss');

            if (!momentDate.isValid()) {
                return Promise.reject(new Error('Wrong date format. Should be YYYY-MM-DD HH:mm:ss'));
            }
        }

        if (filename != null && dateUpdated != null && disableCache === false) {
            utils.logDebugInfo(`Looking for cached file ${filename}`);
            momentDate = moment(dateUpdated, 'YYYY-MM-DD HH:mm:ss');

            if (!momentDate.isValid()) {
                return Promise.reject(new Error('Wrong date format.'));
            }

            const key = PDFCreator._getBucketFileName(filename, momentDate, false);
            const fileExists = await this._checkIfFileExistsAndUnchanged(key, momentDate);

            if (fileExists) {
                utils.logDebugInfo(`Found in cache.`);

                return new Promise((resolve) => {
                    resolve(this._getSignedURL(key));
                });
            }
        }

        filename = filename || PDFCreator._generateFilename();

        const wkhtmlOptions = {
            ...options,
            ...this.defaultRendererOptions,
        };

        const key = PDFCreator._getBucketFileName(filename, momentDate, disableCache);
        utils.logDebugInfo(`Filename: ${key}`);


        const tmpFile = `${OUTPUT_TMP_FOLDER}/${key}`;
        const writeStream = fs.createWriteStream(tmpFile);

        utils.logDebugInfo('WKHTMLTOPDF called.');

        return new Promise((resolve, reject) => {
            wkhtmltopdf(content, wkhtmlOptions,
                () => {
                    utils.logDebugInfo('Rendering finished. Uploading to S3');

                    S3.putObject({
                        Bucket: this.s3_bucket,
                        Key: key,
                        Body: fs.createReadStream(tmpFile),
                        ContentType: 'application/pdf',
                        ContentDisposition: `attachment;filename=${key}`,
                    },
                    (error) => {
                        if (error != null) {
                            reject(new Error('Unable to send file to S3'));
                        } else {
                            resolve(this._getSignedURL(key));
                        }
                    });
                }).pipe(writeStream);
        });
    }

    /**
     *
     * @param {String} key
     * @param {Object} dateUpdated formatted with Moment
     * @return {Promise<boolean>}
     * @private
     */
    _checkIfFileExistsAndUnchanged(key, dateUpdated) {
        return new Promise((resolve) => {
            S3.headObject({
                Bucket: this.s3_bucket,
                Key: key,
            }, (error, response) => {
                if (error != null || response == null) {
                    return resolve(false);
                }
                // This check is actually redundant since we store the date in the filename
                // but in case we change the naming conventions, it should remain working
                const lastModified = moment(response.LastModified, 'ddd, DD MMM YYYY HH:mm:ss ZZ');
                return resolve(lastModified >= dateUpdated);
            });
        });
    }

    /**
     * Returns URL of file uploaded to S3 bucket valid for expiryTime seconds
     * @param {String} key of object in S3 bucket
     * @return {string} URL
     * @private
     */
    _getSignedURL(key) {
        return S3.getSignedUrl('getObject', {
            Bucket: this.s3_bucket,
            Key: key,
            Expires: this.expiryTime,
        });
    }

    /**
     * Return standardized filename in specific for this S3 bucket
     * @param {String} filename
     * @param {Object} dateUpdated MomentJS datetime
     * @param {Boolean} shouldAddNoCachePostfix Should file be available for caching?
     * @return {string}
     * @private
     */
    static _getBucketFileName(filename, dateUpdated, shouldAddNoCachePostfix) {
        return `${filename}-${dateUpdated.format(FILENAME_DATE_FORMAT)}${shouldAddNoCachePostfix ? '-nocache' : ''}.pdf`;
    }

    /**
     * Generate random file name for pdf
     * @return {string} PDF file name
     * @private
     */
    static _generateFilename() {
        return `${uuid.v4()}`;
    }
}

module.exports = PDFCreator;
