const config = require('../config');

/**
 * Log if debug is enabled
 * @param {String} message
 */
const logDebugInfo = (message) => {
    if (config.debug) {
        console.log(`[HTML-TO-PDF] ${message}`);
    }
};

/**
 * Log error
 * @param {String} message
 */
const logError = (message) => {
    console.log(`[HTML-TO-PDF] ${message}`);
};


/**
 * Error object to return on failure
 * @param {Object} event
 * @param {String} errorType
 * @param {Number} statusCode
 * @param {String} message
 * @return {{errorType: *, httpStatus: *, message: *}}
 */
const getErrorResponse = (event, errorType, statusCode, message) => {
    return getResponse(event, {
        errorType: errorType,
        message: message,
    }, statusCode);
};

/**
 * Generate response for request
 * @param {Object} event Event from lambda function (for host header)
 * @param {Object} body
 * @param {Number} statusCode
 * @param {Object} headers
 * @return {Object} response
 */
const getResponse = (event, body, statusCode = 200, headers = {}) => {
    const origin = event.headers.origin;

    const re = new RegExp(config.originDomainsRegex, 'ig');

    const additionalHeaders = {
        'Access-Control-Allow-Credentials': true,
        'Access-Control-Allow-Origin': 'null',
        'Content-Type': 'application/json',
    };

    if (re.test(origin)) {
        additionalHeaders['Access-Control-Allow-Origin'] = origin;
    }

    const mergedHeaders = {
        ...additionalHeaders,
        ...headers,
    };

    return {
        statusCode: statusCode,
        headers: mergedHeaders,
        body: JSON.stringify(body, null, 2),
    };
};

/**
 * Convert HTTP Headers to format supported by WKHTMLTOPDF (as an options record)
 * @param {Object} headers
 * @return {Object} Custom Header Options for WKHTMLTOPDF
 */
const transformHeadersToOptions = (headers) => {
    const options = [];

    for (const key in headers) {
        if (!headers.hasOwnProperty(key)) {
            continue;
        }

        options.push([key, headers[key]]);
    }

    return {
        customHeader: options,
    };
};

/**
 * Convert cookies to format supported by WKHTMLTOPDF (as an options record)
 * @param {Object} cookies
 * @return {Object} Custom Cookies Options for WKHTMLTOPDF
 */
const transformCookiesToOptions = (cookies) => {
    const options = [];

    for (const key in cookies) {
        if (!cookies.hasOwnProperty(key)) {
            continue;
        }

        options.push([key, cookies[key]]);
    }

    return {
        cookie: options,
    };
};

module.exports = {
    getErrorResponse,
    getResponse,
    logDebugInfo,
    logError,
    transformHeadersToOptions,
    transformCookiesToOptions,
};
