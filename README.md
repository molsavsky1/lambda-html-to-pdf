# AWS Lambda - HTML to PDF

## Stack:
- Serverless
- S3 (the AWS Lambda response has 6MB limit + S3 allows caching)

## Deploy
1) Change configuration in serverless.yml -> environment variables (Project name, S3 bucket name)
2) Update `config.js` if needed
3) Run `serverless deploy -y`
4) Copy important endpoint URLs

## REST API

### / [POST]

+ **Request** (application/json)

  + Headers:
      
      `Content-Type: application/json`
  
  + Body:
  
      ```json5
      {
        "content": "HTML OR URL", // Make sure to inline all styles when using pure HTML
        "filename": "",    // Optional - Generated UUID v4 if not specified,
        "dateUpdated": "YYYY-MM-DD HH:mm:ss", // Optional - Required for caching (sample format: 2019-03-03 20:20:20)
        "disableCache": 1, // Optional (0 by default)
        "shouldRedirect": 0, // Optional (0 by default) - Should return 301 status code with redirect to URL instead of returning JSON with URL
        "forceHeaders": {}, // Optional ({} by default) - Which headers should be passed with the rendering request?
        "forceCookies": {} // Optional ({} by default) - Which cookies should be passed with the rendering request?
      }
      ```
  
#### Responses

--------------------------------------------------------------------------------

+ **Response 200** (application/json)

    + Body
    
        ```json5
        {
          "url": "https://SampleUrlToS3BBucketValidForXMinutes" // S3 Presigned URL
        }
        ```

--------------------------------------------------------------------------------
    
+ **Response 301** (application/json) 

        -> Redirect to S3 Presigned URL (if shouldRedirect == 1)
    
--------------------------------------------------------------------------------

+ **Response 500** (application/json)

    + Body:
    
        ```json5
        {
          "errorType": "RenderError", // Error category  
          "message":  "Detail error message"
        }
        ```
        
--------------------------------------------------------------------------------

+ **Response 400** (application/json)
    
    + Body:
    
        ```json5
        {
          "errorType": "ParseError", // Error category  
          "message":  "Invalid JSON in request Body"
        }
        ```
        
--------------------------------------------------------------------------------

+ **Response 400** (application/json)
    
    + Body:
    
        ```json5
        {
          "errorType": "MissingData", // Error category  
          "message":  "Some data is missing in the request body"
        }
        ```

--------------------------------------------------------------------------------

+ **Response 400** (application/json)
    
    + Body:
    
        ```json5
        {
          "errorType": "InvalidArgument",
          "message": "Invalid parameter value forceHeaders or forceCookies (should be object)"
        }
        ```
