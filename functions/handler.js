'use strict';

const config = require('../config');
const utils = require('../src/utils');
const PDFCreator = require('../src/pdfCreator');

process.env['PATH'] = `${process.env['PATH']}:${process.env['LAMBDA_TASK_ROOT']}/bin`;

const pdfCreator = new PDFCreator(config.bucket, config.signedUrlExpiresSeconds, config.wkhtmlOptions);

module.exports.htmlToPdf = async (event, context) => {
    /** Immediate response for WarmUP plugin */
    if (event.source === 'serverless-plugin-warmup') {
        utils.logDebugInfo('WarmUP - Lambda is warm!');

        return utils.getResponse(
            event, {
                'message': 'Lambda is warm!',
            });
    }

    utils.logDebugInfo('Request to render content');

    let body;

    try {
        body = JSON.parse(event.body);
    } catch (e) {
        utils.logError('Parse error.');

        return utils.getErrorResponse(
            event,
            'ParseError',
            400,
            'Invalid JSON in request body',
        );
    }

    if (!body || !body.content) {
        utils.logError(`Missing content ${JSON.stringify(body)}`);

        return utils.getErrorResponse(
            event,
            'MissingData',
            400,
            'Some data is missing in the request body',
        );
    }

    const dateUpdated = body.dateUpdated;
    const shouldRedirect = body.shouldRedirect || false;
    const disableCaching = body.disableCache || false;
    const forceHeaders = body.forceHeaders || {};
    const forceCookies = body.forceCookies || {};

    if (typeof forceHeaders !== 'object' || typeof forceCookies !== 'object') {
        utils.logError(`Value of forceHeaders or forceCookies is not valid ${JSON.stringify(body)}`);

        return utils.getErrorResponse(
            event,
            'InvalidArgument',
            400,
            'Invalid parameter value of forceHeaders or forceCookies (should be object)',
        );
    }

    const filename = `${body.filename || Math.random().toString(36).slice(2)}`;
    const pageSize = body.pageSize || 'a4';

    utils.logDebugInfo('[HTML-TO-PDF] Rendering...');

    const options = {
        pageSize: pageSize,
        ...utils.transformHeadersToOptions(forceHeaders),
        ...utils.transformCookiesToOptions(forceCookies),
    };

    try {
        const url = await pdfCreator.render(body.content, filename, dateUpdated, disableCaching, options);

        utils.logDebugInfo(`[HTML-TO-PDF] Generated url: ${url}`);

        if (shouldRedirect) {
            return utils.getResponse({}, 301, {'Location': url});
        } else {
            return utils.getResponse(event, {url: url});
        }
    } catch (error) {
        utils.logError(`[HTML-TO-PDF] ERROR: ${error}`);
        return utils.getErrorResponse(
            event,
            'RenderError',
            500,
            error,
        );
    }
};
