module.exports = {
    originDomainsRegex: convertStringToCorsRegex(process.env.ORIGIN_DOMAINS),
    bucket: process.env.LAMBDA_BUCKET,
    debug: process.env.DEBUG,
    signedUrlExpiresSeconds: 5 * 60,

    wkhtmlOptions: {
        'marginTop': '1cm',
        'marginBottom': '1cm',
        'marginLeft': '1cm',
        'marginRight': '1cm',
        'enableSmartShrinking': true,
        // web: {
        //     enableIntelligentShrinking: true,
        // },
    },
};

/**
 * Convert string in format xxx,yyy to regex like cors validation string
 * @param {String} str
 * @return {RegExp}
 */
function convertStringToCorsRegex(str) {
    // join each domain with "OR(|)"
    // replace all regex significant characters with escaped alternatives
    const reStr = str
        .split(',')
        .map((domain) => `(${domain})`)
        .join('|')
        .replace(/\//ig, '\\/')
        .replace(/\./ig, '\\.')
        .replace(/\*/ig, '.*');

    return new RegExp(reStr, 'ig');
}
